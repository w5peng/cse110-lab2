/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author AlbertP
 *
 */
public class Celsius extends Temperature{

	/**
	 * 
	 */
	public Celsius(float t) {
		super(t);
	}
	public String toString(){
		
		return String.valueOf(this.getValue()) + " C";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius(this.getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(this.getValue()*9/5+32);
	}

}
